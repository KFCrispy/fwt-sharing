<?php

// First wave (07/09/2017)

$heroes_mark["Chris"][0]["name"]               = "Lagrangian's Will";
$heroes_mark["Chris"][0]["img"]                = "xx.png";
$heroes_mark["Chris"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Chris"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Chris"][0]["effect"]             = [
    "label" => "If 2 or more Paper-type Heroes are in the party, Casts Protect, taking 8% incoming damage for all allies", 
];

$heroes_mark["Ian"][0]["name"]               = "The Most Normal Cat";
$heroes_mark["Ian"][0]["img"]                = "xx.png";
$heroes_mark["Ian"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Ian"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Ian"][0]["effect"]             = [
    "label" => "If 3 or more allies are alive, increases their Mastery by 2000 and Attack by 10%", 
];

$heroes_mark["Jenny"][0]["name"]               = "Wild West Girl";
$heroes_mark["Jenny"][0]["img"]                = "xx.png";
$heroes_mark["Jenny"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Jenny"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Jenny"][0]["effect"]             = [
    "label" => "If 2 or more Thornbush-type Heroes are in the party, increases all allies' Critical Damage by 5%", 
];

$heroes_mark["Lena"][0]["name"]               = "Lena on Stage";
$heroes_mark["Lena"][0]["img"]                = "xx.png";
$heroes_mark["Lena"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Lena"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Lena"][0]["effect"]             = [
    "label" => "If 3 or more Scissors-type Heroes are in the party, increases all allies' Attack by 2400", 
];

$heroes_mark["Klein"][0]["name"]               = "Tragic Idealist";
$heroes_mark["Klein"][0]["img"]                = "xx.png";
$heroes_mark["Klein"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Klein"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Klein"][0]["effect"]             = [
    "label" => "If 2 or more Area attack-type Heroes are in the party, decreases all allies' skill MP costs by 15%", 
];

$heroes_mark["Belle"][0]["name"]               = "Gentle Smiling Healer";
$heroes_mark["Belle"][0]["img"]                = "xx.png";
$heroes_mark["Belle"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Belle"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Belle"][0]["effect"]             = [
    "label" => "Increases all allies' Max HP by 7%. Nirvana in the same party increases Belle's Attack by 20%", 
];

$heroes_mark["Celestial"][0]["name"]               = "Dragon's Restored Hapiness";
$heroes_mark["Celestial"][0]["img"]                = "xx.png";
$heroes_mark["Celestial"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Celestial"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Celestial"][0]["effect"]             = [
    "label" => "If there is Yeka or Yekaterina in the party, restores all allies' MP by 15 every turn (up to 30)", 
];

$heroes_mark["Deimos"][0]["name"]               = "Blood-soaked Empire";
$heroes_mark["Deimos"][0]["img"]                = "xx.png";
$heroes_mark["Deimos"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Deimos"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Deimos"][0]["effect"]             = [
    "label" => "If 2 or more Attack-type Heroes are in the party, increases all allies' Movement by 1 for 1 turn and 2 turns for Deimos", 
];

$heroes_mark["Hongyeom"][0]["name"]               = "Blazing Flower of the East";
$heroes_mark["Hongyeom"][0]["img"]                = "xx.png";
$heroes_mark["Hongyeom"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Hongyeom"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Hongyeom"][0]["effect"]             = [
    "label" => "If 3 or more Rock-type heroes are in the party, increases all allies' Terrain Strategy Power by 5%", 
];

$heroes_mark["Thanatos"][0]["name"]               = "The Promise That Couldn't Be Kept";
$heroes_mark["Thanatos"][0]["img"]                = "xx.png";
$heroes_mark["Thanatos"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Thanatos"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Thanatos"][0]["effect"]             = [
    "label" => "If 2 or more Balance-type Heroes are in the party, increases all allies' Final Skill Power by 10%", 
];

$heroes_mark["Alfred"][0]["name"]               = "Ketarh's Great Master";
$heroes_mark["Alfred"][0]["img"]                = "xx.png";
$heroes_mark["Alfred"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Alfred"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Alfred"][0]["effect"]             = [
    "label" => "If 3 or more allies are alive, increases all allies' Max HP by 7%", 
];

$heroes_mark["Sogoon"][0]["name"]               = "Young Tiger of Snowfields";
$heroes_mark["Sogoon"][0]["img"]                = "xx.png";
$heroes_mark["Sogoon"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Sogoon"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Sogoon"][0]["effect"]             = [
    "label" => "If 2 or more Ice Terrain Type Heroes are in the party, increases all allies' Terrain Strategy Power by 5%", 
];

$heroes_mark["Rage"][0]["name"]               = "Stylish Gunner";
$heroes_mark["Rage"][0]["img"]                = "xx.png";
$heroes_mark["Rage"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Rage"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Rage"][0]["effect"]             = [
    "label" => "If 2 or more Attack Type Heroes are in the party, increases all allies' Attack by 2400", 
];

$heroes_mark["Chenny"][0]["name"]               = "Dangerous Genius Girl";
$heroes_mark["Chenny"][0]["img"]                = "xx.png";
$heroes_mark["Chenny"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Chenny"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Chenny"][0]["effect"]             = [
    "label" => "If 3 or more Balance Type Heroes are in the party, increases all allies' Critical Damage by 5%", 
];

$heroes_mark["Shu-Shu"][0]["name"]               = "Small yet Great Girl";
$heroes_mark["Shu-Shu"][0]["img"]                = "xx.png";
$heroes_mark["Shu-Shu"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Shu-Shu"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Shu-Shu"][0]["effect"]             = [
    "label" => "If 4 or more allies are alive, increases all allies' Max HP by 5% every turn", 
];

$heroes_mark["Nox"][0]["name"]               = "Drowsy Vampire";
$heroes_mark["Nox"][0]["img"]                = "xx.png";
$heroes_mark["Nox"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Nox"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Nox"][0]["effect"]             = [
    "label" => "If 2 or more Paper-type Heroes are in the party, increases all allies' Resistance by 5%", 
];

$heroes_mark["Phantom"][0]["name"]               = "Hero Phantom Thief";
$heroes_mark["Phantom"][0]["img"]                = "xx.png";
$heroes_mark["Phantom"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Phantom"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Phantom"][0]["effect"]             = [
    "label" => "If Luminous is in the party, all allies' Movement increases by 1 for 2 turns, and Phantom increases for 3 turns", 
];

$heroes_mark["Evan"][0]["name"]               = "Dragon Master's Successor";
$heroes_mark["Evan"][0]["img"]                = "xx.png";
$heroes_mark["Evan"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Evan"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Evan"][0]["effect"]             = [
    "label" => "If Aran is in the party, all Final Skill Power increases by 10%", 
];

$heroes_mark["Luminous"][0]["name"]               = "Destiny's Foe";
$heroes_mark["Luminous"][0]["img"]                = "xx.png";
$heroes_mark["Luminous"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Luminous"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Luminous"][0]["effect"]             = [
    "label" => "If Phantom is in the party, all received damage by allies decreases by 5% for 1 turn, and Luminous decreases by 2 turns", 
];

$heroes_mark["Eunwol"][0]["name"]               = "Hero in the Shadows";
$heroes_mark["Eunwol"][0]["img"]                = "xx.png";
$heroes_mark["Eunwol"][0]["stats"]["Attack"]    = 380;
$heroes_mark["Eunwol"][0]["stats"]["Crit Rate"] = 200;
$heroes_mark["Eunwol"][0]["effect"]             = [
    "label" => "If Evan is in the party, all allies' Max HP increases by 10000", 
];