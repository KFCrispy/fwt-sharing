<?php

$hero_costumes["Alex"][0]["name"]                              = "Dragon Knight Alex";
$hero_costumes["Alex"][0]["img"]                               = "alex-0.png";
$hero_costumes["Alex"][0]["stats"]["Attack"]                   = 380;
$hero_costumes["Alex"][0]["stats"]["Hit Rate"]                 = 50;

$hero_costumes["Alex"][1]["name"]                              = "Savage Warrior Alex";
$hero_costumes["Alex"][1]["img"]                               = "alex-1.png";
$hero_costumes["Alex"][1]["stats"]["HP"]                       = 900;
$hero_costumes["Alex"][1]["stats"]["Crit Rate"]                = 100;

$hero_costumes["Alfred"][0]["name"]                            = "Alfred of Olympus";
$hero_costumes["Alfred"][0]["img"]                             = "alfred-0.png";
$hero_costumes["Alfred"][0]["stats"]["HP"]                     = 900;
$hero_costumes["Alfred"][0]["stats"]["Attack"]                 = 190;

$hero_costumes["Alfred"][1]["name"]                            = "Terrifying Principal Alfred";
$hero_costumes["Alfred"][1]["img"]                             = "alfred-1.png";
$hero_costumes["Alfred"][1]["stats"]["Counter Damage"]         = 450;
$hero_costumes["Alfred"][1]["stats"]["Counter Rate"]           = 100;

$hero_costumes["Amora"][0]["name"]                             = "Goddess of the Desert Amora";
$hero_costumes["Amora"][0]["img"]                              = "hero-costume-x.png";
$hero_costumes["Amora"][0]["stats"]["HP"]                      = 1500;
$hero_costumes["Amora"][0]["stats"]["Movement"]                = 1;

$hero_costumes["Angela"][0]["name"]                            = "Snowflake Girl Angela";
$hero_costumes["Angela"][0]["img"]                             = "angela-0.png";
$hero_costumes["Angela"][0]["stats"]["HP"]                     = 300;
$hero_costumes["Angela"][0]["stats"]["Movement"]               = 1;

$hero_costumes["Angela"][1]["name"]                            = "Heart Queen in Wonderland";
$hero_costumes["Angela"][1]["img"]                             = "hero-costume-x.png";
$hero_costumes["Angela"][1]["stats"]["Attack"]                 = 475;
$hero_costumes["Angela"][1]["stats"]["Crit Rate"]              = 250;

$hero_costumes["Angela"][2]["name"]                            = "Christmas Winter Princess Angela";
$hero_costumes["Angela"][2]["img"]                             = "hero-costume-x.png";
$hero_costumes["Angela"][2]["stats"]["Attack"]                 = 380;
$hero_costumes["Angela"][2]["stats"]["Defense"]                = 100;

$hero_costumes["Aran"][0]["name"]                              = "Snow Leopard Aran";
$hero_costumes["Aran"][0]["img"]                               = "hero-costume-x.png";
$hero_costumes["Aran"][0]["stats"]["HP"]                       = 600;
$hero_costumes["Aran"][0]["stats"]["Attack"]                   = 285;

$hero_costumes["Azrael"][0]["name"]                            = "Artificial Demon Azrael";
$hero_costumes["Azrael"][0]["img"]                             = "azrael-0.png";
$hero_costumes["Azrael"][0]["stats"]["Crit Rate"]              = 100;
$hero_costumes["Azrael"][0]["stats"]["Counter Damage"]         = 300;

$hero_costumes["Azrael"][1]["name"]                            = "Surgeon John";
$hero_costumes["Azrael"][1]["img"]                             = "hero-costume-x.png";
$hero_costumes["Azrael"][1]["stats"]["HP"]                     = 1500;
$hero_costumes["Azrael"][1]["stats"]["MP Recovery"]            = 5;

$hero_costumes["Banshee"][0]["name"]                           = "Banshee in Wonderland";
$hero_costumes["Banshee"][0]["img"]                            = "banshee-0.png";
$hero_costumes["Banshee"][0]["stats"]["Attack"]                = 190;
$hero_costumes["Banshee"][0]["stats"]["Crit Rate"]             = 150;

$hero_costumes["Banshee"][1]["name"]                           = "Maid Twins Banshee";
$hero_costumes["Banshee"][1]["img"]                            = "banshee-1.png";
$hero_costumes["Banshee"][1]["stats"]["Defense"]               = 300;
$hero_costumes["Banshee"][1]["stats"]["Hit Rate"]              = 100;

$hero_costumes["Bearman"][0]["name"]                           = "Bear Robot Vehicle";
$hero_costumes["Bearman"][0]["img"]                            = "bearman-0.png";
$hero_costumes["Bearman"][0]["stats"]["Attack"]                = 190;
$hero_costumes["Bearman"][0]["stats"]["Hit Rate"]              = 150;

$hero_costumes["Belle"][0]["name"]                             = "Gisaeng Wolhyang";
$hero_costumes["Belle"][0]["img"]                              = "belle-0.png";
$hero_costumes["Belle"][0]["stats"]["HP"]                      = 900;
$hero_costumes["Belle"][0]["stats"]["Attack"]                  = 190;

$hero_costumes["Belle"][1]["name"]                             = "Priestess Belle";
$hero_costumes["Belle"][1]["img"]                              = "hero-costume-x.png";
$hero_costumes["Belle"][1]["stats"]["Attack"]                  = 285;
$hero_costumes["Belle"][1]["stats"]["Hit Rate"]                = 100;

$hero_costumes["Camelia"][0]["name"]                           = "Night Camelia";
$hero_costumes["Camelia"][0]["img"]                            = "hero-costume-x.png";
$hero_costumes["Camelia"][0]["stats"]["HP"]                    = 1500;
$hero_costumes["Camelia"][0]["stats"]["MP Recovery"]           = 5;

$hero_costumes["Camelia"][1]["name"]                           = "Dawn Cotton";
$hero_costumes["Camelia"][1]["img"]                            = "hero-costume-x.png";
$hero_costumes["Camelia"][1]["stats"]["Attack"]                = 475;
$hero_costumes["Camelia"][1]["stats"]["Crit Rate"]             = 250;

$hero_costumes["Camilla"][0]["name"]                           = "Private School Camilla";
$hero_costumes["Camilla"][0]["img"]                            = "hero-costume-x.png";
$hero_costumes["Camilla"][0]["stats"]["Attack"]                = 475;
$hero_costumes["Camilla"][0]["stats"]["Defense"]               = 500;

$hero_costumes["Carrot"][0]["name"]                            = "Ni-hao Carrot";
$hero_costumes["Carrot"][0]["img"]                             = "carrot-0.png";
$hero_costumes["Carrot"][0]["stats"]["HP"]                     = 1600;
$hero_costumes["Carrot"][0]["stats"]["Hit Rate"]               = 50;

$hero_costumes["Carrot"][1]["name"]                            = "X-mas Snowman Carrot";
$hero_costumes["Carrot"][1]["img"]                             = "carrot-1.png";
$hero_costumes["Carrot"][1]["stats"]["Dodge"]                  = 200;
$hero_costumes["Carrot"][1]["stats"]["MP Recovery"]            = 4;

$hero_costumes["Carrot"][2]["name"]                            = "Chesire Cat in Wonderland";
$hero_costumes["Carrot"][2]["img"]                             = "hero-costume-x.png";
$hero_costumes["Carrot"][2]["stats"]["Attack"]                 = 475;
$hero_costumes["Carrot"][2]["stats"]["Defense"]                = 500;

$hero_costumes["Cat Sidhe"][0]["name"]                         = "Wandering Swordsman Cat Sidhe";
$hero_costumes["Cat Sidhe"][0]["img"]                          = "cat-sidhe-0.png";
$hero_costumes["Cat Sidhe"][0]["stats"]["Attack"]              = 285;
$hero_costumes["Cat Sidhe"][0]["stats"]["Hit Rate"]            = 100;

$hero_costumes["Celestial"][0]["name"]                         = "Velvet Mini Celestial";
$hero_costumes["Celestial"][0]["img"]                          = "celestial-0.png";
$hero_costumes["Celestial"][0]["stats"]["Defense"]             = 200;
$hero_costumes["Celestial"][0]["stats"]["Crit Rate"]           = 150;

$hero_costumes["Celestial"][1]["name"]                         = "Spider Queen Celestial";
$hero_costumes["Celestial"][1]["img"]                          = "celestial-1.png";
$hero_costumes["Celestial"][1]["stats"]["Attack"]              = 285;
$hero_costumes["Celestial"][1]["stats"]["Defense"]             = 200;

$hero_costumes["Celestial"][2]["name"]                         = "Snowflake Hanbok Celestial";
$hero_costumes["Celestial"][2]["img"]                          = "celestial-2.png";
$hero_costumes["Celestial"][2]["stats"]["Attack"]              = 285;
$hero_costumes["Celestial"][2]["stats"]["Defense"]             = 200;

$hero_costumes["Celestial"][3]["name"]                         = "Moon Dust";
$hero_costumes["Celestial"][3]["img"]                          = "hero-costume-x.png";
$hero_costumes["Celestial"][3]["stats"]["HP"]                  = 1800;
$hero_costumes["Celestial"][3]["stats"]["Defense"]             = 600;

$hero_costumes["Chenny"][0]["name"]                            = "Bunny Girl Chenny";
$hero_costumes["Chenny"][0]["img"]                             = "chenny-0.png";
$hero_costumes["Chenny"][0]["stats"]["HP"]                     = 900;
$hero_costumes["Chenny"][0]["stats"]["Dodge"]                  = 100;

$hero_costumes["Chenny"][1]["name"]                            = "Native Girl Chenny";
$hero_costumes["Chenny"][1]["img"]                             = "chenny-1.png";
$hero_costumes["Chenny"][1]["stats"]["Attack"]                 = 285;
$hero_costumes["Chenny"][1]["stats"]["Hit Rate"]               = 100;

$hero_costumes["Chenny"][2]["name"]                            = "Grim Reaper Chenny";
$hero_costumes["Chenny"][2]["img"]                             = "chenny-2.png";
$hero_costumes["Chenny"][2]["stats"]["HP"]                     = 900;
$hero_costumes["Chenny"][2]["stats"]["MP Recovery"]            = 2;

$hero_costumes["Chenny"][2]["name"]                            = "Bear Candry Chenny";
$hero_costumes["Chenny"][2]["img"]                             = "hero-costume-x.png";
$hero_costumes["Chenny"][2]["stats"]["Defense"]                = 400;
$hero_costumes["Chenny"][2]["stats"]["Crit Rate"]              = 200;

$hero_costumes["Chris"][0]["name"]                             = "Genius Athlete Chris";
$hero_costumes["Chris"][0]["img"]                              = "chris-0.png";
$hero_costumes["Chris"][0]["stats"]["HP"]                      = 900;
$hero_costumes["Chris"][0]["stats"]["Attack"]                  = 190;

$hero_costumes["Chris"][1]["name"]                             = "Pirate Queen Chris";
$hero_costumes["Chris"][1]["img"]                              = "chris-1.png";
$hero_costumes["Chris"][1]["stats"]["HP"]                      = 450;
$hero_costumes["Chris"][1]["stats"]["Attack"]                  = 95;

$hero_costumes["Chris"][2]["name"]                             = "Santa Chris";
$hero_costumes["Chris"][2]["img"]                              = "chris-2.png";
$hero_costumes["Chris"][2]["stats"]["Crit Rate"]               = 200;
$hero_costumes["Chris"][2]["stats"]["Hit Rate"]                = 200;

$hero_costumes["Chris"][3]["name"]                             = "Chris, Your Eminence";
$hero_costumes["Chris"][3]["img"]                              = "chris-3.png";
$hero_costumes["Chris"][3]["stats"]["MP"]                      = 16;
$hero_costumes["Chris"][3]["stats"]["Defense"]                 = 300;

$hero_costumes["Chris"][4]["name"]                             = "Warrior Chris";
$hero_costumes["Chris"][4]["img"]                              = "chris-4.png";
$hero_costumes["Chris"][4]["stats"]["MP"]                      = 16;
$hero_costumes["Chris"][4]["stats"]["Defense"]                 = 300;

$hero_costumes["Churyeok"][0]["name"]                          = "Benevolent King Churyeok";
$hero_costumes["Churyeok"][0]["img"]                           = "hero-costume-x.png";
$hero_costumes["Churyeok"][0]["stats"]["Attack"]               = 475;
$hero_costumes["Churyeok"][0]["stats"]["Crit Rate"]            = 250;

$hero_costumes["Cleo"][0]["name"]                              = "First Grade, Room 4 Cleo";
$hero_costumes["Cleo"][0]["img"]                               = "cleo-0.png";
$hero_costumes["Cleo"][0]["stats"]["Crit Rate"]                = 150;
$hero_costumes["Cleo"][0]["stats"]["Dodge"]                    = 100;

$hero_costumes["Cleo"][1]["name"]                              = "Cleo the Seductive Succubus";
$hero_costumes["Cleo"][1]["img"]                               = "cleo-1.png";
$hero_costumes["Cleo"][1]["stats"]["HP"]                       = 600;
$hero_costumes["Cleo"][1]["stats"]["Attack"]                   = 285;

$hero_costumes["Deborah"][0]["name"]                           = "Deborah on the Beach";
$hero_costumes["Deborah"][0]["img"]                            = "deborah-0.png";
$hero_costumes["Deborah"][0]["stats"]["Hit Rate"]              = 150;
$hero_costumes["Deborah"][0]["stats"]["Mastery"]               = 400;

$hero_costumes["Deborah"][1]["name"]                           = "X-mas Rabbit Deborah";
$hero_costumes["Deborah"][1]["img"]                            = "deborah-1.png";
$hero_costumes["Deborah"][1]["stats"]["Attack"]                = 380;
$hero_costumes["Deborah"][1]["stats"]["Dodge"]                 = 200;

$hero_costumes["Deborah"][2]["name"]                           = "Red Riding Hood Deborah";
$hero_costumes["Deborah"][2]["img"]                            = "hero-costume-x.png";
$hero_costumes["Deborah"][2]["stats"]["HP"]                    = 900;
$hero_costumes["Deborah"][2]["stats"]["Defense"]               = 300;

$hero_costumes["Deimos"][0]["name"]                            = "Berserk Deimos";
$hero_costumes["Deimos"][0]["img"]                             = "deimos-0.png";
$hero_costumes["Deimos"][0]["stats"]["HP"]                     = 1200;
$hero_costumes["Deimos"][0]["stats"]["Defense"]                = 100;

$hero_costumes["Deimos"][1]["name"]                            = "Vampire Hunter Derrick";
$hero_costumes["Deimos"][1]["img"]                             = "hero-costume-x.png";
$hero_costumes["Deimos"][1]["stats"]["Mastery"]                = 1000;
$hero_costumes["Deimos"][1]["stats"]["MP Recovery"]            = 5;

$hero_costumes["Dolores"][0]["name"]                           = "Dolores on the Beach";
$hero_costumes["Dolores"][0]["img"]                            = "dolores-0.png";
$hero_costumes["Dolores"][0]["stats"]["Crit Rate"]             = 100;
$hero_costumes["Dolores"][0]["stats"]["MP Recovery"]           = 3;

$hero_costumes["Dolores"][1]["name"]                           = "Tooth Fairy Dolores";
$hero_costumes["Dolores"][1]["img"]                            = "dolores-1.png";
$hero_costumes["Dolores"][1]["stats"]["Attack"]                = 285;
$hero_costumes["Dolores"][1]["stats"]["Defense"]               = 200;

$hero_costumes["Dominique"][0]["name"]                         = "Kunoichi Dominique";
$hero_costumes["Dominique"][0]["img"]                          = "dominique-0.png";
$hero_costumes["Dominique"][0]["stats"]["MP"]                  = 24;
$hero_costumes["Dominique"][0]["stats"]["Crit Rate"]           = 100;

$hero_costumes["Dominique"][1]["name"]                         = "Kendo Club Dominique";
$hero_costumes["Dominique"][1]["img"]                          = "hero-costume-x.png";
$hero_costumes["Dominique"][1]["stats"]["HP"]                  = 900;
$hero_costumes["Dominique"][1]["stats"]["Dodge"]               = 100;

$hero_costumes["Elektra"][0]["name"]                           = "Hot Queen Elektra";
$hero_costumes["Elektra"][0]["img"]                            = "elektra-0.png";
$hero_costumes["Elektra"][0]["stats"]["Attack"]                = 190;
$hero_costumes["Elektra"][0]["stats"]["Defense"]               = 300;

$hero_costumes["Eunwol"][0]["name"]                            = "Anima Tribe Eunwol";
$hero_costumes["Eunwol"][0]["img"]                             = "eunwol-0.png";
$hero_costumes["Eunwol"][0]["stats"]["Attack"]                 = 380;
$hero_costumes["Eunwol"][0]["stats"]["Defense"]                = 100;

$hero_costumes["Evan"][0]["name"]                              = "Marine Boy Evan";
$hero_costumes["Evan"][0]["img"]                               = "evan-0.png";
$hero_costumes["Evan"][0]["stats"]["Attack"]                   = 190;
$hero_costumes["Evan"][0]["stats"]["Defense"]                  = 300;

$hero_costumes["Frankenstein"][0]["name"]                      = "Demon Frankenstein";
$hero_costumes["Frankenstein"][0]["img"]                       = "frankenstein-0.png";
$hero_costumes["Frankenstein"][0]["stats"]["Attack"]           = 150;
$hero_costumes["Frankenstein"][0]["stats"]["Hit Rate"]         = 400;

$hero_costumes["Frankenstein"][1]["name"]                      = "Freed Frankenstein";
$hero_costumes["Frankenstein"][1]["img"]                       = "frankenstein-1.png";
$hero_costumes["Frankenstein"][1]["stats"]["HP"]               = 600;
$hero_costumes["Frankenstein"][1]["stats"]["Defense"]          = 300;

$hero_costumes["Fruel"][0]["name"]                             = "Archery Club Fruel";
$hero_costumes["Fruel"][0]["img"]                              = "fruel-0.png";
$hero_costumes["Fruel"][0]["stats"]["Hit Rate"]                = 150;
$hero_costumes["Fruel"][0]["stats"]["Mastery"]                 = 400;

$hero_costumes["Gillan"][0]["name"]                            = "Struggling Student Gillan";
$hero_costumes["Gillan"][0]["img"]                             = "gillan-0.png";
$hero_costumes["Gillan"][0]["stats"]["Counter Damage"]         = 300;
$hero_costumes["Gillan"][0]["stats"]["Counter Rate"]           = 150;

$hero_costumes["Gillan"][1]["name"]                            = "Black White Master Gillan";
$hero_costumes["Gillan"][1]["img"]                             = "gillan-1.png";
$hero_costumes["Gillan"][1]["stats"]["MP"]                     = 24;
$hero_costumes["Gillan"][1]["stats"]["Counter Rate"]           = 150;

$hero_costumes["Gillan"][2]["name"]                            = "Red Talisman Gillan";
$hero_costumes["Gillan"][2]["img"]                             = "hero-costume-x.png";
$hero_costumes["Gillan"][2]["stats"]["Attack"]                 = 285;
$hero_costumes["Gillan"][2]["stats"]["Defense"]                = 200;

$hero_costumes["Hella"][0]["name"]                             = "Night Queen Hella";
$hero_costumes["Hella"][0]["img"]                              = "hella-0.png";
$hero_costumes["Hella"][0]["stats"]["Defense"]                 = 200;
$hero_costumes["Hella"][0]["stats"]["Dodge"]                   = 150;

$hero_costumes["Henry"][0]["name"]                             = "Robot Machine Henry";
$hero_costumes["Henry"][0]["img"]                              = "henry-0.png";
$hero_costumes["Henry"][0]["stats"]["Hit Rate"]                = 100;
$hero_costumes["Henry"][0]["stats"]["MP Recovery"]             = 3;

$hero_costumes["Heuksa"][0]["name"]                            = "Warlord Wijung";
$hero_costumes["Heuksa"][0]["img"]                             = "hero-costume-x.png";
$hero_costumes["Heuksa"][0]["stats"]["HP"]                     = 1500;
$hero_costumes["Heuksa"][0]["stats"]["Defense"]                = 500;

$hero_costumes["Hongyeom"][0]["name"]                          = "Dancer Hongyeom";
$hero_costumes["Hongyeom"][0]["img"]                           = "hongyeom-0.png";
$hero_costumes["Hongyeom"][0]["stats"]["Attack"]               = 475;
$hero_costumes["Hongyeom"][0]["stats"]["Crit Rate"]            = 250;

$hero_costumes["Ian"][0]["name"]                               = "Cat Pirate Captain Ian";
$hero_costumes["Ian"][0]["img"]                                = "ian-0.png";
$hero_costumes["Ian"][0]["stats"]["Attack"]                    = 190;
$hero_costumes["Ian"][0]["stats"]["Dodge"]                     = 150;

$hero_costumes["Ian"][1]["name"]                               = "Kitty Marine Ian";
$hero_costumes["Ian"][1]["img"]                                = "ian-1.png";
$hero_costumes["Ian"][1]["stats"]["HP"]                        = 450;
$hero_costumes["Ian"][1]["stats"]["Attack"]                    = 95;

$hero_costumes["Ian"][2]["name"]                               = "Knight Ian";
$hero_costumes["Ian"][2]["img"]                                = "ian-2.png";
$hero_costumes["Ian"][2]["stats"]["Attack"]                    = 285;
$hero_costumes["Ian"][2]["stats"]["Hit Rate"]                  = 100;

$hero_costumes["Ian"][3]["name"]                               = "Dragon's Dignity Ian";
$hero_costumes["Ian"][3]["img"]                                = "ian-3.png";
$hero_costumes["Ian"][3]["stats"]["HP"]                        = 600;
$hero_costumes["Ian"][3]["stats"]["Attack"]                    = 285;

$hero_costumes["Ian"][4]["name"]                               = "Dark Warrior Ian";
$hero_costumes["Ian"][4]["img"]                                = "hero-costume-x.png";
$hero_costumes["Ian"][4]["stats"]["MP"]                        = 40;
$hero_costumes["Ian"][4]["stats"]["Defense"]                   = 500;

$hero_costumes["Ildo"][0]["name"]                              = "Street Fighter Ildo";
$hero_costumes["Ildo"][0]["img"]                               = "ildo-0.png";
$hero_costumes["Ildo"][0]["stats"]["Defense"]                  = 500;
$hero_costumes["Ildo"][0]["stats"]["Counter Damage"]           = 750;

$hero_costumes["Jack"][0]["name"]                              = "Weretiger Jack";
$hero_costumes["Jack"][0]["img"]                               = "jack-0.png";
$hero_costumes["Jack"][0]["stats"]["Attack"]                   = 285;
$hero_costumes["Jack"][0]["stats"]["Counter Rate"]             = 100;

$hero_costumes["Jack"][1]["name"]                              = "X-mas Gentleman Jack";
$hero_costumes["Jack"][1]["img"]                               = "jack-1.png";
$hero_costumes["Jack"][1]["stats"]["Hit Rate"]                 = 250;
$hero_costumes["Jack"][1]["stats"]["Counter Damage"]           = 450;

$hero_costumes["Jenny"][0]["name"]                             = "Special Agent Jenny";
$hero_costumes["Jenny"][0]["img"]                              = "jenny-0.png";
$hero_costumes["Jenny"][0]["stats"]["Crit Rate"]               = 100;
$hero_costumes["Jenny"][0]["stats"]["Counter Damage"]          = 450;

$hero_costumes["Jenny"][1]["name"]                             = "Valentine's Kiss Jenny";
$hero_costumes["Jenny"][1]["img"]                              = "jenny-1.png";
$hero_costumes["Jenny"][1]["stats"]["Attack"]                  = 285;
$hero_costumes["Jenny"][1]["stats"]["Mastery"]                 = 200;

$hero_costumes["Jin"][0]["name"]                               = "Mechanic Jin in Shorts";
$hero_costumes["Jin"][0]["img"]                                = "jin-0.png";
$hero_costumes["Jin"][0]["stats"]["HP"]                        = 900;
$hero_costumes["Jin"][0]["stats"]["Counter Damage"]            = 300;

$hero_costumes["Jin"][1]["name"]                               = "Dark Warrior Jin";
$hero_costumes["Jin"][1]["img"]                                = "hero-costume-x.png";
$hero_costumes["Jin"][1]["stats"]["Attack"]                    = 475;
$hero_costumes["Jin"][1]["stats"]["MP Recovery"]               = 5;

$hero_costumes["Jin"][2]["name"]                               = "Mechanic Kayla";
$hero_costumes["Jin"][2]["img"]                                = "hero-costume-x.png";
$hero_costumes["Jin"][2]["stats"]["Defense"]                   = 400;
$hero_costumes["Jin"][2]["stats"]["Counter Damage"]            = 600;

$hero_costumes["Jinyo"][0]["name"]                             = "Blue Night's Dokkaebi";
$hero_costumes["Jinyo"][0]["img"]                              = "hero-costume-x.png";
$hero_costumes["Jinyo"][0]["stats"]["HP"]                      = 1500;
$hero_costumes["Jinyo"][0]["stats"]["Defense"]                 = 500;

$hero_costumes["Jin Kisaragi"][0]["name"]                      = "Black Edition Jin";
$hero_costumes["Jin Kisaragi"][0]["img"]                       = "jin-kisaragi-0.png";
$hero_costumes["Jin Kisaragi"][0]["stats"]["Attack"]           = 285;
$hero_costumes["Jin Kisaragi"][0]["stats"]["Defense"]          = 200;

$hero_costumes["Kai"][0]["name"]                               = "4th Batter Kai";
$hero_costumes["Kai"][0]["img"]                                = "kai-0.png";
$hero_costumes["Kai"][0]["stats"]["HP"]                        = 600;
$hero_costumes["Kai"][0]["stats"]["Crit Rate"]                 = 150;

$hero_costumes["Kai"][1]["name"]                               = "Heaven's Champion Kai";
$hero_costumes["Kai"][1]["img"]                                = "kai-1.png";
$hero_costumes["Kai"][1]["stats"]["MP"]                        = 24;
$hero_costumes["Kai"][1]["stats"]["Attack"]                    = 190;

$hero_costumes["Kitty"][0]["name"]                             = "Neighbor Yumin";
$hero_costumes["Kitty"][0]["img"]                              = "kitty-0.png";
$hero_costumes["Kitty"][0]["stats"]["HP"]                      = 1200;
$hero_costumes["Kitty"][0]["stats"]["Crit Rate"]               = 50;

$hero_costumes["Kitty"][1]["name"]                             = "Kitty Kitty";
$hero_costumes["Kitty"][1]["img"]                              = "kitty-1.png";
$hero_costumes["Kitty"][1]["stats"]["Attack"]                  = 190;
$hero_costumes["Kitty"][1]["stats"]["Defense"]                 = 300;

$hero_costumes["Klein"][0]["name"]                             = "Thanatos Klein";
$hero_costumes["Klein"][0]["img"]                              = "klein-0.png";
$hero_costumes["Klein"][0]["stats"]["Counter Damage"]          = 450;
$hero_costumes["Klein"][0]["stats"]["Counter Rate"]            = 100;

$hero_costumes["Krut"][0]["name"]                              = "Krut the Black Reaper";
$hero_costumes["Krut"][0]["img"]                               = "krut-0.png";
$hero_costumes["Krut"][0]["stats"]["HP"]                       = 600;
$hero_costumes["Krut"][0]["stats"]["Attack"]                   = 285;

$hero_costumes["Krut"][1]["name"]                              = "Zombie Krut";
$hero_costumes["Krut"][1]["img"]                               = "krut-1.png";
$hero_costumes["Krut"][1]["stats"]["HP"]                       = 900;
$hero_costumes["Krut"][1]["stats"]["Attack"]                   = 190;

$hero_costumes["Krut"][2]["name"]                              = "Squeaky Mousie";
$hero_costumes["Krut"][2]["img"]                               = "krut-2.png";
$hero_costumes["Krut"][2]["stats"]["HP"]                       = 900;
$hero_costumes["Krut"][2]["stats"]["Attack"]                   = 285;

$hero_costumes["Ky Kiske"][0]["name"]                          = "Black Edition Ky Kiske";
$hero_costumes["Ky Kiske"][0]["img"]                           = "hero-costume-x.png";
$hero_costumes["Ky Kiske"][0]["stats"]["MP"]                   = 40;
$hero_costumes["Ky Kiske"][0]["stats"]["Defense"]              = 500;

$hero_costumes["Lance"][0]["name"]                             = "Dragon Fighter Lance";
$hero_costumes["Lance"][0]["img"]                              = "lance-0.png";
$hero_costumes["Lance"][0]["stats"]["HP"]                      = 900;
$hero_costumes["Lance"][0]["stats"]["Defense"]                 = 200;

$hero_costumes["Lance"][1]["name"]                             = "Super Robot Lance";
$hero_costumes["Lance"][1]["img"]                              = "lance-1.png";
$hero_costumes["Lance"][1]["stats"]["Attack"]                  = 95;
$hero_costumes["Lance"][1]["stats"]["Mastery"]                 = 800;

$hero_costumes["Lance"][2]["name"]                             = "Ultra Transformer Lance";
$hero_costumes["Lance"][2]["img"]                              = "hero-costume-x.png";
$hero_costumes["Lance"][2]["stats"]["Crit Rate"]               = 200;
$hero_costumes["Lance"][2]["stats"]["Hit Rate"]                = 200;

$hero_costumes["Lee"][0]["name"]                               = "Midsummer Lee";
$hero_costumes["Lee"][0]["img"]                                = "lee-0.png";
$hero_costumes["Lee"][0]["stats"]["Counter Damage"]            = 600;
$hero_costumes["Lee"][0]["stats"]["MP Recovery"]               = 1;

$hero_costumes["Lee"][1]["name"]                               = "Chi Master Lee";
$hero_costumes["Lee"][1]["img"]                                = "hero-costume-x.png";
$hero_costumes["Lee"][1]["stats"]["Attack"]                    = 95;
$hero_costumes["Lee"][1]["stats"]["Crit Rate"]                 = 200;

$hero_costumes["Lena"][0]["name"]                              = "Bride Lena";
$hero_costumes["Lena"][0]["img"]                               = "lena-0.png";
$hero_costumes["Lena"][0]["stats"]["HP"]                       = 900;
$hero_costumes["Lena"][0]["stats"]["Attack"]                   = 190;

$hero_costumes["Lena"][1]["name"]                              = "Ghost Bride Lena";
$hero_costumes["Lena"][1]["img"]                               = "lena-1.png";
$hero_costumes["Lena"][1]["stats"]["MP"]                       = 24;
$hero_costumes["Lena"][1]["stats"]["Crit Rate"]                = 100;

$hero_costumes["Lena"][2]["name"]                              = "Summer Concert Lena";
$hero_costumes["Lena"][2]["img"]                               = "lena-2.png";
$hero_costumes["Lena"][2]["stats"]["Defense"]                  = 500;
$hero_costumes["Lena"][2]["stats"]["MP Recovery"]              = 5;

$hero_costumes["Lena"][3]["name"]                              = "Red Planet";
$hero_costumes["Lena"][3]["img"]                               = "hero-costume-x.png";
$hero_costumes["Lena"][3]["stats"]["MP"]                       = 24;
$hero_costumes["Lena"][3]["stats"]["Crit Rate"]                = 100;

$hero_costumes["Lena"][4]["name"]                              = "World Star Lena";
$hero_costumes["Lena"][4]["img"]                               = "hero-costume-x.png";
$hero_costumes["Lena"][4]["stats"]["Defense"]                  = 300;
$hero_costumes["Lena"][4]["stats"]["Hit Rate"]                 = 100;

$hero_costumes["Lilid"][0]["name"]                             = "Panda Boy Lilid";
$hero_costumes["Lilid"][0]["img"]                              = "lilid-0.png";
$hero_costumes["Lilid"][0]["stats"]["HP"]                      = 600;
$hero_costumes["Lilid"][0]["stats"]["Attack"]                  = 285;

$hero_costumes["Lily"][0]["name"]                              = "Gentle Butler Lily";
$hero_costumes["Lily"][0]["img"]                               = "lily-0.png";
$hero_costumes["Lily"][0]["stats"]["Crit Rate"]                = 100;
$hero_costumes["Lily"][0]["stats"]["Counter Damage"]           = 450;

$hero_costumes["Lily"][1]["name"]                              = "X-mas Elf Lily";
$hero_costumes["Lily"][1]["img"]                               = "lily-1.png";
$hero_costumes["Lily"][1]["stats"]["MP"]                       = 24;
$hero_costumes["Lily"][1]["stats"]["Crit Rate"]                = 250;

$hero_costumes["Lucas"][0]["name"]                             = "Police Officer Lucas";
$hero_costumes["Lucas"][0]["img"]                              = "hero-costume-x.png";
$hero_costumes["Lucas"][0]["stats"]["HP"]                      = 900;
$hero_costumes["Lucas"][0]["stats"]["Crit Rate"]               = 100;

$hero_costumes["Lucas"][1]["name"]                             = "Gangbuster Lucas";
$hero_costumes["Lucas"][1]["img"]                              = "hero-costume-x.png";
$hero_costumes["Lucas"][1]["stats"]["Attack"]                  = 380;
$hero_costumes["Lucas"][1]["stats"]["Defense"]                 = 400;

$hero_costumes["Luminous"][0]["name"]                          = "Winter Boy Luminous";
$hero_costumes["Luminous"][0]["img"]                           = "luminous-0.png";
$hero_costumes["Luminous"][0]["stats"]["MP"]                   = 32;
$hero_costumes["Luminous"][0]["stats"]["Mastery"]              = 200;

$hero_costumes["Mary"][0]["name"]                              = "Lady Mary";
$hero_costumes["Mary"][0]["img"]                               = "hero-costume-x.png";
$hero_costumes["Mary"][0]["stats"]["Attack"]                   = 475;
$hero_costumes["Mary"][0]["stats"]["Crit Rate"]                = 250;

$hero_costumes["Mary"][1]["name"]                              = "Mary Christmas";
$hero_costumes["Mary"][1]["img"]                               = "hero-costume-x.png";
$hero_costumes["Mary"][1]["stats"]["HP"]                       = 1500;
$hero_costumes["Mary"][1]["stats"]["MP Recovery"]              = 5;

$hero_costumes["Mas"][0]["name"]                               = "Dark Musician Mas";
$hero_costumes["Mas"][0]["img"]                                = "mas-0.png";
$hero_costumes["Mas"][0]["stats"]["Attack"]                    = 285;
$hero_costumes["Mas"][0]["stats"]["Defense"]                   = 200;

$hero_costumes["Mas"][1]["name"]                               = "Hawaiian Mas";
$hero_costumes["Mas"][1]["img"]                                = "mas-1.png";
$hero_costumes["Mas"][1]["stats"]["HP"]                        = 600;
$hero_costumes["Mas"][1]["stats"]["Attack"]                    = 285;

$hero_costumes["Mas"][2]["name"]                               = "Rudolf Mas";
$hero_costumes["Mas"][2]["img"]                                = "mas-2.png";
$hero_costumes["Mas"][2]["stats"]["Crit Rate"]                 = 250;
$hero_costumes["Mas"][2]["stats"]["Mastery"]                   = 600;

$hero_costumes["Mas"][3]["name"]                               = "Zombie Mas";
$hero_costumes["Mas"][3]["img"]                                = "mas-3.png";
$hero_costumes["Mas"][3]["stats"]["HP"]                        = 900;
$hero_costumes["Mas"][3]["stats"]["Attack"]                    = 190;

$hero_costumes["May"][0]["name"]                               = "Black Edition May";
$hero_costumes["May"][0]["img"]                                = "hero-costume-x.png";
$hero_costumes["May"][0]["stats"]["MP"]                        = 40;
$hero_costumes["May"][0]["stats"]["Counter Damage"]            = 750;

$hero_costumes["Mercedes"][0]["name"]                          = "Cheerleader Captain Mercedes";
$hero_costumes["Mercedes"][0]["img"]                           = "mercedes-0.png";
$hero_costumes["Mercedes"][0]["stats"]["MP"]                   = 8;
$hero_costumes["Mercedes"][0]["stats"]["Attack"]               = 380;

$hero_costumes["Moa"][0]["name"]                               = "Raccoon Mage Moa";
$hero_costumes["Moa"][0]["img"]                                = "moa-0.png";
$hero_costumes["Moa"][0]["stats"]["MP"]                        = 24;
$hero_costumes["Moa"][0]["stats"]["Mastery"]                   = 400;

$hero_costumes["Momo"][0]["name"]                              = "Tin Rabbit Momo";
$hero_costumes["Momo"][0]["img"]                               = "momo-0.png";
$hero_costumes["Momo"][0]["stats"]["Attack"]                   = 285;
$hero_costumes["Momo"][0]["stats"]["Crit Rate"]                = 100;

$hero_costumes["Momo"][1]["name"]                              = "Tuxedo Momo";
$hero_costumes["Momo"][1]["img"]                               = "momo-1.png";
$hero_costumes["Momo"][1]["stats"]["Hit Rate"]                 = 150;
$hero_costumes["Momo"][1]["stats"]["MP Recovery"]              = 2;

$hero_costumes["Momo"][2]["name"]                              = "White Rabbit in Wonderland";
$hero_costumes["Momo"][2]["img"]                               = "hero-costume-x.png";
$hero_costumes["Momo"][2]["stats"]["MP"]                       = 40;
$hero_costumes["Momo"][2]["stats"]["Counter Damage"]           = 750;

$hero_costumes["Mu"][0]["name"]                                = "Hipster Mu";
$hero_costumes["Mu"][0]["img"]                                 = "mu-0.png";
$hero_costumes["Mu"][0]["stats"]["Crit Rate"]                  = 100;
$hero_costumes["Mu"][0]["stats"]["Mastery"]                    = 600;

$hero_costumes["Mu"][1]["name"]                                = "Senior Mu";
$hero_costumes["Mu"][1]["img"]                                 = "mu-1.png";
$hero_costumes["Mu"][1]["stats"]["HP"]                         = 600;
$hero_costumes["Mu"][1]["stats"]["Defense"]                    = 300;

$hero_costumes["Muang"][0]["name"]                             = "Flame Rockstar Muang";
$hero_costumes["Muang"][0]["img"]                              = "muang-0.png";
$hero_costumes["Muang"][0]["stats"]["HP"]                      = 600;
$hero_costumes["Muang"][0]["stats"]["Mastery"]                 = 400;

$hero_costumes["Muang"][1]["name"]                             = "Hot Pink for the Rocker";
$hero_costumes["Muang"][1]["img"]                              = "hero-costume-x.png";
$hero_costumes["Muang"][1]["stats"]["Defense"]                 = 400;
$hero_costumes["Muang"][1]["stats"]["Crit Rate"]               = 200;

$hero_costumes["Muzaka"][0]["name"]                            = "Freed Muzaka";
$hero_costumes["Muzaka"][0]["img"]                             = "muzaka-0.png";
$hero_costumes["Muzaka"][0]["stats"]["HP"]                     = 600;
$hero_costumes["Muzaka"][0]["stats"]["Defense"]                = 300;

$hero_costumes["Muzaka"][1]["name"]                            = "Dark Shadow Muzaka";
$hero_costumes["Muzaka"][1]["img"]                             = "muzaka-1.png";
$hero_costumes["Muzaka"][1]["stats"]["Attack"]                 = 285;
$hero_costumes["Muzaka"][1]["stats"]["Hit Rate"]               = 100;

$hero_costumes["Nirvana"][0]["name"]                           = "Flame Rockstar Nirvana";
$hero_costumes["Nirvana"][0]["img"]                            = "nirvana-0.png";
$hero_costumes["Nirvana"][0]["stats"]["Crit Rate"]             = 50;
$hero_costumes["Nirvana"][0]["stats"]["Movement"]              = 1;

$hero_costumes["Nirvana"][1]["name"]                           = "Detective Holmes";
$hero_costumes["Nirvana"][1]["img"]                            = "hero-costume-x.png";
$hero_costumes["Nirvana"][1]["stats"]["Attack"]                = 475;
$hero_costumes["Nirvana"][1]["stats"]["Counter Rate"]          = 250;

$hero_costumes["Noel"][0]["name"]                              = "Black Edition Noel";
$hero_costumes["Noel"][0]["img"]                               = "noel-0.png";
$hero_costumes["Noel"][0]["stats"]["Defense"]                  = 300;
$hero_costumes["Noel"][0]["stats"]["MP Recovery"]              = 2;

$hero_costumes["Nox"][0]["name"]                               = "Fox Mask Nox";
$hero_costumes["Nox"][0]["img"]                                = "nox-0.png";
$hero_costumes["Nox"][0]["stats"]["Hit Rate"]                  = 50;
$hero_costumes["Nox"][0]["stats"]["MP Recovery"]               = 2;

$hero_costumes["Nox"][1]["name"]                               = "Inspector Gerard";
$hero_costumes["Nox"][1]["img"]                                = "hero-costume-x.png";
$hero_costumes["Nox"][1]["stats"]["Attack"]                    = 475;
$hero_costumes["Nox"][1]["stats"]["Dodge"]                     = 250;

$hero_costumes["Nox"][2]["name"]                               = "Dark Night Foxfire";
$hero_costumes["Nox"][2]["img"]                                = "hero-costume-x.png";
$hero_costumes["Nox"][2]["stats"]["HP"]                        = 1200;
$hero_costumes["Nox"][2]["stats"]["Defense"]                   = 400;

$hero_costumes["Parsifal"][0]["name"]                          = "Midsummer Parsifal";
$hero_costumes["Parsifal"][0]["img"]                           = "hero-costume-x.png";
$hero_costumes["Parsifal"][0]["stats"]["Attack"]               = 475;
$hero_costumes["Parsifal"][0]["stats"]["Crit Rate"]            = 250;

$hero_costumes["Persona"][0]["name"]                           = "Black Cape Persona";
$hero_costumes["Persona"][0]["img"]                            = "persona-0.png";
$hero_costumes["Persona"][0]["stats"]["Dodge"]                 = 150;
$hero_costumes["Persona"][0]["stats"]["Counter Rate"]          = 100;

$hero_costumes["Persona"][1]["name"]                           = "Mad Hatter in Wonderland";
$hero_costumes["Persona"][1]["img"]                            = "hero-costume-x.png";
$hero_costumes["Persona"][1]["stats"]["Defense"]               = 500;
$hero_costumes["Persona"][1]["stats"]["Counter Rate"]          = 250;

$hero_costumes["Phantom"][0]["name"]                           = "Winter Boy Phantom";
$hero_costumes["Phantom"][0]["img"]                            = "phantom-0.png";
$hero_costumes["Phantom"][0]["stats"]["Crit Rate"]             = 150;
$hero_costumes["Phantom"][0]["stats"]["Mastery"]               = 400;

$hero_costumes["Pink Bean"][0]["name"]                         = "Cyborg Pink Bean";
$hero_costumes["Pink Bean"][0]["img"]                          = "hero-costume-x.png";
$hero_costumes["Pink Bean"][0]["stats"]["Attack"]              = 475;
$hero_costumes["Pink Bean"][0]["stats"]["Crit Rate"]           = 250;

$hero_costumes["Poni"][0]["name"]                              = "Handsome Rooster Poni";
$hero_costumes["Poni"][0]["img"]                               = "poni-0.png";
$hero_costumes["Poni"][0]["stats"]["HP"]                       = 1500;

$hero_costumes["Poni"][1]["name"]                              = "Mecha Bird Poni";
$hero_costumes["Poni"][1]["img"]                               = "poni-1.png";
$hero_costumes["Poni"][1]["stats"]["HP"]                       = 900;
$hero_costumes["Poni"][1]["stats"]["Defense"]                  = 200;

$hero_costumes["Raboff"][0]["name"]                            = "Black and Gold Raboff";
$hero_costumes["Raboff"][0]["img"]                             = "raboff-0.png";
$hero_costumes["Raboff"][0]["stats"]["Attack"]                 = 285;
$hero_costumes["Raboff"][0]["stats"]["Hit Rate"]               = 100;

$hero_costumes["Rachel"][0]["name"]                            = "Black Edition Rachel";
$hero_costumes["Rachel"][0]["img"]                             = "rachel-0.png";
$hero_costumes["Rachel"][0]["stats"]["Attack"]                 = 285;
$hero_costumes["Rachel"][0]["stats"]["Mastery"]                = 400;

$hero_costumes["Rage"][0]["name"]                              = "High-Top Rage";
$hero_costumes["Rage"][0]["img"]                               = "rage-0.png";
$hero_costumes["Rage"][0]["stats"]["Defense"]                  = 300;
$hero_costumes["Rage"][0]["stats"]["MP Recovery"]              = 2;

$hero_costumes["Rage"][1]["name"]                              = "SWAT Team Rage";
$hero_costumes["Rage"][1]["img"]                               = "rage-1.png";
$hero_costumes["Rage"][1]["stats"]["Attack"]                   = 475;
$hero_costumes["Rage"][1]["stats"]["Mastery"]                  = 1000;

$hero_costumes["Rage"][2]["name"]                              = "Blonde Jun";
$hero_costumes["Rage"][2]["img"]                               = "hero-costume-x.png";
$hero_costumes["Rage"][2]["stats"]["HP"]                       = 1200;
$hero_costumes["Rage"][2]["stats"]["Dodge"]                    = 200;

$hero_costumes["Ragna"][0]["name"]                             = "Black Edition Ragna";
$hero_costumes["Ragna"][0]["img"]                              = "ragna-0.png";
$hero_costumes["Ragna"][0]["stats"]["HP"]                      = 900;
$hero_costumes["Ragna"][0]["stats"]["Crit Rate"]               = 100;

$hero_costumes["Raizel"][0]["name"]                            = "Extracurriculum Raizel";
$hero_costumes["Raizel"][0]["img"]                             = "raizel-0.png";
$hero_costumes["Raizel"][0]["stats"]["Attack"]                 = 285;
$hero_costumes["Raizel"][0]["stats"]["Defense"]                = 200;

$hero_costumes["Ramlethal Valentine"][0]["name"]               = "Black Edition Ramlethal Valentine";
$hero_costumes["Ramlethal Valentine"][0]["img"]                = "hero-costume-x.png";
$hero_costumes["Ramlethal Valentine"][0]["stats"]["Attack"]    = 475;
$hero_costumes["Ramlethal Valentine"][0]["stats"]["Crit Rate"] = 250;

$hero_costumes["Raskreia"][0]["name"]                          = "Ye Ran High School Raskreia";
$hero_costumes["Raskreia"][0]["img"]                           = "raskreia-0.png";
$hero_costumes["Raskreia"][0]["stats"]["HP"]                   = 600;
$hero_costumes["Raskreia"][0]["stats"]["Defense"]              = 300;

$hero_costumes["Raskreia"][1]["name"]                          = "Captain Raskreia";
$hero_costumes["Raskreia"][1]["img"]                           = "raskreia-1.png";
$hero_costumes["Raskreia"][1]["stats"]["Attack"]               = 285;
$hero_costumes["Raskreia"][1]["stats"]["Hit Rate"]             = 100;

$hero_costumes["Reina"][0]["name"]                             = "Dragon Hunter Reina";
$hero_costumes["Reina"][0]["img"]                              = "reina-0.png";
$hero_costumes["Reina"][0]["stats"]["HP"]                      = 600;
$hero_costumes["Reina"][0]["stats"]["Attack"]                  = 285;

$hero_costumes["Ryeogang"][0]["name"]                          = "Rouge Ryeogang";
$hero_costumes["Ryeogang"][0]["img"]                           = "ryeogang-0.png";
$hero_costumes["Ryeogang"][0]["stats"]["HP"]                   = 1500;
$hero_costumes["Ryeogang"][0]["stats"]["Crit Rate"]            = 250;

$hero_costumes["Seira"][0]["name"]                             = "Ye Ran High School Seira";
$hero_costumes["Seira"][0]["img"]                              = "seira-0.png";
$hero_costumes["Seira"][0]["stats"]["HP"]                      = 600;
$hero_costumes["Seira"][0]["stats"]["Defense"]                 = 300;

$hero_costumes["Seira"][1]["name"]                             = "Deathly Shadow Seira";
$hero_costumes["Seira"][1]["img"]                              = "seira-1.png";
$hero_costumes["Seira"][1]["stats"]["Attack"]                  = 285;
$hero_costumes["Seira"][1]["stats"]["Hit Rate"]                = 100;

$hero_costumes["Serendi"][0]["name"]                           = "Today's Chef Serendi";
$hero_costumes["Serendi"][0]["img"]                            = "serendi-0.png";
$hero_costumes["Serendi"][0]["stats"]["Defense"]               = 100;
$hero_costumes["Serendi"][0]["stats"]["Movement"]              = 1;

$hero_costumes["Serendi"][1]["name"]                           = "Angelic Serendi";
$hero_costumes["Serendi"][1]["img"]                            = "serendi-1.png";
$hero_costumes["Serendi"][1]["stats"]["Attack"]                = 285;
$hero_costumes["Serendi"][1]["stats"]["Crit Rate"]             = 100;

$hero_costumes["Serendi"][2]["name"]                           = "Dark Angel Serendi";
$hero_costumes["Serendi"][2]["img"]                            = "hero-costume-x.png";
$hero_costumes["Serendi"][2]["stats"]["HP"]                    = 1200;
$hero_costumes["Serendi"][2]["stats"]["Attack"]                = 380;

$hero_costumes["Serphina"][0]["name"]                          = "Brawler Serphina";
$hero_costumes["Serphina"][0]["img"]                           = "serphina-0.png";
$hero_costumes["Serphina"][0]["stats"]["Attack"]               = 285;
$hero_costumes["Serphina"][0]["stats"]["Hit Rate"]             = 100;

$hero_costumes["Shark"][0]["name"]                             = "Admiral Shark";
$hero_costumes["Shark"][0]["img"]                              = "shark-0.png";
$hero_costumes["Shark"][0]["stats"]["Counter Damage"]          = 300;
$hero_costumes["Shark"][0]["stats"]["Counter Rate"]            = 150;

$hero_costumes["Shark"][1]["name"]                             = "Devil Man Shark";
$hero_costumes["Shark"][1]["img"]                              = "shark-1.png";
$hero_costumes["Shark"][1]["stats"]["HP"]                      = 1500;
$hero_costumes["Shark"][1]["stats"]["Crit Rate"]               = 250;

$hero_costumes["Shu Shu"][0]["name"]                           = "Nurse Officer Shu-Shu";
$hero_costumes["Shu Shu"][0]["img"]                            = "shu-shu-0.png";
$hero_costumes["Shu Shu"][0]["stats"]["HP"]                    = 900;
$hero_costumes["Shu Shu"][0]["stats"]["Attack"]                = 190;

$hero_costumes["Sione"][0]["name"]                             = "Officer Sione";
$hero_costumes["Sione"][0]["img"]                              = "sione-0.png";
$hero_costumes["Sione"][0]["stats"]["HP"]                      = 600;
$hero_costumes["Sione"][0]["stats"]["Counter Damage"]          = 450;

$hero_costumes["Sione"][1]["name"]                             = "Night and Sakura Sione";
$hero_costumes["Sione"][1]["img"]                              = "hero-costume-x.png";
$hero_costumes["Sione"][1]["stats"]["Attack"]                  = 475;
$hero_costumes["Sione"][1]["stats"]["Dodge"]                   = 250;

$hero_costumes["Sogoon"][0]["name"]                            = "Boy Warrior Heukun";
$hero_costumes["Sogoon"][0]["img"]                             = "hero-costume-x.png";
$hero_costumes["Sogoon"][0]["stats"]["Defense"]                = 500;
$hero_costumes["Sogoon"][0]["stats"]["Counter Rate"]           = 250;

$hero_costumes["Sol Badguy"][0]["name"]                        = "Black Edition Sol Badguy";
$hero_costumes["Sol Badguy"][0]["img"]                         = "hero-costume-x.png";
$hero_costumes["Sol Badguy"][0]["stats"]["HP"]                 = 1500;
$hero_costumes["Sol Badguy"][0]["stats"]["MP Recovery"]        = 5;

$hero_costumes["Sonic Boom"][0]["name"]                        = "Mecha Ninja Sonic Boom";
$hero_costumes["Sonic Boom"][0]["img"]                         = "sonic-boom-0.png";
$hero_costumes["Sonic Boom"][0]["stats"]["HP"]                 = 600;
$hero_costumes["Sonic Boom"][0]["stats"]["Mastery"]            = 450;

$hero_costumes["Sonic Boom"][1]["name"]                        = "Skull King Sonic Boom";
$hero_costumes["Sonic Boom"][1]["img"]                         = "sonic-boom-1.png";
$hero_costumes["Sonic Boom"][1]["stats"]["Attack"]             = 475;
$hero_costumes["Sonic Boom"][1]["stats"]["Crit Rate"]          = 250;

$hero_costumes["Spooky"][0]["name"]                            = "Spooky in Wonderland";
$hero_costumes["Spooky"][0]["img"]                             = "spooky-0.png";
$hero_costumes["Spooky"][0]["stats"]["HP"]                     = 600;
$hero_costumes["Spooky"][0]["stats"]["Attack"]                 = 285;

$hero_costumes["Spooky"][1]["name"]                            = "Maid Twins Spooky";
$hero_costumes["Spooky"][1]["img"]                             = "spooky-1.png";
$hero_costumes["Spooky"][1]["stats"]["HP"]                     = 900;
$hero_costumes["Spooky"][1]["stats"]["Dodge"]                  = 100;

$hero_costumes["Sraka"][0]["name"]                             = "13th Day Sraka";
$hero_costumes["Sraka"][0]["img"]                              = "sraka-0.png";
$hero_costumes["Sraka"][0]["stats"]["HP"]                      = 600;
$hero_costumes["Sraka"][0]["stats"]["Defense"]                 = 300;

$hero_costumes["Taehwa"][0]["name"]                            = "Count Oscar";
$hero_costumes["Taehwa"][0]["img"]                             = "taehwa-0.png";
$hero_costumes["Taehwa"][0]["stats"]["HP"]                     = 1500;
$hero_costumes["Taehwa"][0]["stats"]["MP Recovery"]            = 5;

$hero_costumes["Tao"][0]["name"]                               = "Boxer Girl Tao";
$hero_costumes["Tao"][0]["img"]                                = "tao-0.png";
$hero_costumes["Tao"][0]["stats"]["HP"]                        = 600;
$hero_costumes["Tao"][0]["stats"]["Defense"]                   = 300;

$hero_costumes["Tao"][1]["name"]                               = "Cool Vacation Tao";
$hero_costumes["Tao"][1]["img"]                                = "hero-costume-x.png";
$hero_costumes["Tao"][1]["stats"]["Attack"]                    = 285;
$hero_costumes["Tao"][1]["stats"]["Crit Rate"]                 = 100;

$hero_costumes["Thanatos"][0]["name"]                          = "Dragon's Fury";
$hero_costumes["Thanatos"][0]["img"]                           = "thanatos-0.png";
$hero_costumes["Thanatos"][0]["stats"]["Attack"]               = 380;
$hero_costumes["Thanatos"][0]["stats"]["MP Recovery"]          = 2;

$hero_costumes["Unknown"][0]["name"]                           = "Tenacious Unknown";
$hero_costumes["Unknown"][0]["img"]                            = "unknown-0.png";
$hero_costumes["Unknown"][0]["stats"]["Crit Rate"]             = 150;
$hero_costumes["Unknown"][0]["stats"]["Hit Rate"]              = 100;

$hero_costumes["Unknown"][1]["name"]                           = "Bad Guy Unknown";
$hero_costumes["Unknown"][1]["img"]                            = "unknown-1.png";
$hero_costumes["Unknown"][1]["stats"]["HP"]                    = 600;
$hero_costumes["Unknown"][1]["stats"]["Counter Damage"]        = 450;

$hero_costumes["Unknown"][2]["name"]                           = "Black Alley Blue Wolf";
$hero_costumes["Unknown"][2]["img"]                            = "hero-costume-x.png";
$hero_costumes["Unknown"][2]["stats"]["Attack"]                = 380;
$hero_costumes["Unknown"][2]["stats"]["Counter Rate"]          = 200;

$hero_costumes["Woryeong"][0]["name"]                          = "Starlight Witch Maerchen";
$hero_costumes["Woryeong"][0]["img"]                           = "hero-costume-x.png";
$hero_costumes["Woryeong"][0]["stats"]["HP"]                   = 1500;
$hero_costumes["Woryeong"][0]["stats"]["Attack"]               = 475;

$hero_costumes["Valkyrie"][0]["name"]                          = "Sailor Style Valkyrie";
$hero_costumes["Valkyrie"][0]["img"]                           = "valkyrie-0.png";
$hero_costumes["Valkyrie"][0]["stats"]["HP"]                   = 1500;

$hero_costumes["Valkyrie"][1]["name"]                          = "Zombie Valkyrie";
$hero_costumes["Valkyrie"][1]["img"]                           = "valkyrie-1.png";
$hero_costumes["Valkyrie"][1]["stats"]["HP"]                   = 900;
$hero_costumes["Valkyrie"][1]["stats"]["Attack"]               = 180;

$hero_costumes["Valkyrie"][2]["name"]                          = "Christmas Rudolph Girl Valkyrie";
$hero_costumes["Valkyrie"][2]["img"]                           = "hero-costume-x.png";
$hero_costumes["Valkyrie"][2]["stats"]["HP"]                   = 600;
$hero_costumes["Valkyrie"][2]["stats"]["Counter Rate"]         = 150;

$hero_costumes["Yeka"][0]["name"]                              = "Alice in Wonderland";
$hero_costumes["Yeka"][0]["img"]                               = "hero-costume-x.png";
$hero_costumes["Yeka"][0]["stats"]["Attack"]                   = 475;
$hero_costumes["Yeka"][0]["stats"]["HP"]                       = 1500;

$hero_costumes["Yeka"][1]["name"]                              = "Baby Chick Yeka";
$hero_costumes["Yeka"][1]["img"]                               = "hero-costume-x.png";
$hero_costumes["Yeka"][1]["stats"]["MP Recovery"]              = 5;
$hero_costumes["Yeka"][1]["stats"]["Movement"]                 = 1;

$hero_costumes["Yeka"][2]["name"]                              = "Black Cat Yeka";
$hero_costumes["Yeka"][2]["img"]                               = "hero-costume-x.png";
$hero_costumes["Yeka"][2]["stats"]["MP"]                       = 40;
$hero_costumes["Yeka"][2]["stats"]["Defense"]                  = 500;

$hero_costumes["Yeka"][3]["name"]                              = "Gold Dragon Yeka";
$hero_costumes["Yeka"][3]["img"]                               = "hero-costume-x.png";
$hero_costumes["Yeka"][3]["stats"]["Attack"]                   = 475;
$hero_costumes["Yeka"][3]["stats"]["Crit Rate"]                = 250;

$hero_costumes["Yeka"][4]["name"]                              = "Dream Weaver";
$hero_costumes["Yeka"][4]["img"]                               = "hero-costume-x.png";
$hero_costumes["Yeka"][4]["stats"]["Crit Rate"]                = 300;
$hero_costumes["Yeka"][4]["stats"]["Dodge"]                    = 300;

$hero_costumes["Yekaterina"][0]["name"]                        = "Velvet Doll Yekaterina";
$hero_costumes["Yekaterina"][0]["img"]                         = "yekaterina-0.png";
$hero_costumes["Yekaterina"][0]["stats"]["HP"]                 = 1200;
$hero_costumes["Yekaterina"][0]["stats"]["Defense"]            = 100;

$hero_costumes["Yekaterina"][1]["name"]                        = "Sandy Beach Yekaterina";
$hero_costumes["Yekaterina"][1]["img"]                         = "hero-costume-x.png";
$hero_costumes["Yekaterina"][1]["stats"]["Attack"]             = 475;
$hero_costumes["Yekaterina"][1]["stats"]["Dodge"]              = 250;

$hero_costumes["Zero"][0]["name"]                              = "High Ninja Zero";
$hero_costumes["Zero"][0]["img"]                               = "zero-0.png";
$hero_costumes["Zero"][0]["stats"]["Attack"]                   = 190;
$hero_costumes["Zero"][0]["stats"]["Dodge"]                    = 150;